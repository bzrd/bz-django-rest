==============
BZ Django Rest
==============

REST-oriented mini-framework for Django inspired by the book "RESTful Webservices Cookbook" by O'Reilly.
