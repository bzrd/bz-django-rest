from rest.views import ResourceView
from django.http import HttpResponse

class CustomResourceView(ResourceView):
    
    documentation = {
        'url' : 'http://www.example.org/docs/photos',
        'content_type': 'text/html',
        'rel' : 'help'
    }

    accepted_media_types = {
        'default': [
            'application/json'
        ],
        'get': [
            'application/json',
            'text/html'
        ]
    }

    def extend_known_methods(self):
        return ['xxx']

    def patch(self, request, *args, **kwargs):
        return HttpResponse('PATCH!')
